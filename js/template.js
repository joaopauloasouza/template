jQuery(document).ready(function($) {
    // Sidebar toggle
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $('.public').tooltip({
        'trigger':'hover',
        'title': 'Público',
        'placement': 'left'
    });

    $('.private').tooltip({
        'trigger':'hover',
        'title': 'Privado',
        'placement': 'left'
    });
});
